# Real-Time-Chat-ReactJs-NodeJs
Real time chat app server side using node.js react.js graphql, apollo-server


### Installation

```sh
$ git clone git@gitlab.com:obenjrtaib/Real-Time-Chat-ReactJs-NodeJs.git
$ cd client
$ yarn install
$ cd server
$ yarn install
```
